#!/usr/bin/env python

from flask import Flask
from flask import request
from flask import abort
from flask import Response
from flask import make_response
import rethinkdb
import json
import re
from bs4 import BeautifulSoup

app = Flask(__name__)

@app.route('/names/<name>', methods=['GET','PUT','DELETE'])
def name_operations(name):
  if request.method == 'GET':
    result = db_get(name)
    if result == None:
      print "Could not find name in db. Returning 404 Not Found..."
      abort(404)
    return Response(json.dumps(result, sort_keys=True, separators=(',',':'))+'\n', mimetype="application/json")
  elif request.method == 'PUT':
    db_insert(name)
  elif request.method == 'DELETE':
    db_delete(name)
  return make_response('', 204) #Request successfully processed, returning no content

@app.route('/names', methods=['DELETE'])
def delete_names():
  db_delete_all()
  return make_response('', 204) #Request successfully processed, returning no content

@app.route('/annotate', methods=['POST'])
def annotate():
  pattern = re.compile(r'([A-Za-z0-9]+)')
  #map sub_name function over each word in the input string.
  if request.data == None:
    print "Could not parse request data. Returning 400 Bad Request..."
    abort(400)
  soup = BeautifulSoup(request.data)
  for txt in soup.findAll(text=pattern):
    if(txt.parent.name!='a'):
      prev_tag = None
      for word in pattern.split(txt):
        url = get_url(word)
        if url != None:
          new_tag = soup.new_tag("a", href=url)
          new_tag.string = word
        else:
          new_tag = soup.new_string(word)
        if prev_tag == None:
          txt.replace_with(new_tag)
        else:
          prev_tag.insert_after(new_tag)
        prev_tag = new_tag
  
  #return as test/plain to avoid XSS problems from reflecting html.
  return Response(soup.encode(), mimetype="text/plain")

def get_url(word):
  mapping = db_get(word)
  #If word is not a known name, the return it unchanged
  if mapping == None: return None
  try:
    url = mapping['url']
  except:
    print "db value malformed (doesn't contain url). Returning 500 Internal Server Error..."
    abort(500)
  return url

def db_get(name):
  try:
    result = db_table.get(name).run(db_conn)
  except:
    print "Couldn't read from db. Returning 500 Internal Server Error..."
    abort(500)
  return result

def db_insert(name):
  try:
    url = request.get_json(force=True)['url']
  except:
    print "Could not parse JSON. Returning 400 Bad Request..."
    abort(400)
  try:
    #insert into table. upsert flag will allow new url value to replace an old one.
    db_table.insert({'name': name,'url': url}, upsert=True).run(db_conn)
  except:
    print "Couldn't write to db. Returning 500 Internal Server Error..."
    abort(500)

def db_delete(name):
  try:
    db_table.get(name).delete().run(db_conn)
  except:
    print "Couldn't delete record from db. Returning 500 Internal Server Error..."
    abort(500)

def db_delete_all():
  try:
    db_table.delete().run(db_conn)
  except:
    print "Couldn't delete all records from db. Returning 500 Internal Server Error..."
    abort(500)

if __name__ == '__main__':
  global db_conn, db_table
  db_conn = rethinkdb.connect()
  db_table = rethinkdb.table('names')
  app.run(debug=True)