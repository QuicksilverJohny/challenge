#!/bin/sh
#Inital setup that needs to run in order to install 3rd party libraries and set up db
#(Assumes python and pip are installed. May need to run as root, depending on pip configuration)
pip install flask
pip install rethinkdb
pip install beautifulsoup4
rethinkdb

python -c "import rethinkdb as r; r.connect(); r.table_create('names',primary_key='name')"